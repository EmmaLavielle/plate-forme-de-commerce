<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Affichage tableau clients</title>
    </head>
    <body>

<?php

# Step 1 : Afficher un tableau d'utilisateurs

$users = require('users.php');

echo 'Liste des clients : ' . '<br><br>';
foreach($users as $client)
{   
    echo 'Id : ' . $client->getId() . '<br>';
    echo 'Email : ' . $client->getEmail() . '<br>';
    echo 'Date de création du compte : ' . $client->getCreatedAt() . '<br><br>';
}

?>

</body>
</html>






