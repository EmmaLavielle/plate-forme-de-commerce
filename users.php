<?php

# Step 1 : Afficher un tableau d'utilisateurs

require_once 'User.php';
require_once 'Client.php';

$client1 = new Client('1', 'client1@gmail.com');
$client2 = new Client('2', 'client2@gmail.com');
$clients = [$client1, $client2];

return $clients;

?>
