<?php

# Step 1 : Afficher un tableau d'utilisateurs

class User
{
    private $_id;
    private $_email;
    private $_createdAt;

    public function __construct($_id, $_email)
    {
        $this->_id = $_id;
        $this->_email = $_email;
        $this->_createdAt = date("d") . "-" . date("m") . "-" . date("Y");
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function getCreatedAt()
    {
        return $this->_createdAt;
    }
}

?>

